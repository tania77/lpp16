#encodign: utf-8
require 'spec_helper'
require 'prct06'

describe Biblioteca do
before :each do
    @l1 = Biblioteca.new("titulo", "autor",  "serie", "editorial", "nedicion", "fecha", "isbn10", "isbn13")
end
it "debe existir un titulo" do
   @l1.titulo.should eq("titulo")
   #spec to 
   
end
it "debe existir al menos un autor" do
    @l1.autor.should eq("autor")
end

it "debe existir o no una serie" do
    @l1.serie.should eq("serie") 
end
it "debe existir una editorial" do
    @l1.editorial.should eq("editorial") 
    @l1.nedicion.should eq("nedicion") 
end
it "debe existir una fecha de publicacion" do
    @l1.fecha.should eq("fecha") 
end
it "debe haber almenos un numero isbn" do
    @l1.isbn10.should eq("isbn10")
    @l1.isbn13.should eq("isbn13")
end

end
